void main() {
  final superman = Heroe('Superman');
  final joker = Villano('El risas');
  
  print(joker.maldad);
  print(superman);
}

abstract class Personaje {
  String? poder;
  String nombre;
  
  Personaje(this.nombre);
  
  @override
  String toString() {
    return '$nombre - $poder';
  }
}

class Heroe extends Personaje {
  Heroe(String nombre): super(nombre);
}

class Villano extends Personaje {
  int maldad = 50;
  
  Villano(String nombre): super(nombre);
  
}