void main() {
  
   final rawJson = {
     'nombre': 'Tony stark',
//      'poder': 'Dinero'
   };
  
  final ironman = Heroe.fromJson(rawJson);
  
  
  print(ironman);
//   final wolverine = Heroe(nombre: 'Peter', poder: 'Tela-araña');
//   print(wolverine);
  
  
}

class Heroe {
  String nombre;
  String poder;

  Heroe({required this.nombre, required this.poder});

  Heroe.fromJson(Map<String, String> json):
    this.nombre = json['nombre'] ?? 'No tiene nombre',
    this.poder = json['poder'] ?? 'No tiene poder';
  
  
  @override
  String toString() {
    return 'nombre: ${this.nombre}, poder: ${this.poder}';
  }
}
