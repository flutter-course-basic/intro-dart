void main() {
  
  // Maps
//   Map persona = {
//     'nombre': 'Juan',
//     'edad': 35,
//     'soltero': false,
//     true: false,
//     1: 100,
//     2: 500
//   };
  
//     print(persona[true]);
//   print(persona[1]);
  
    Map<String, dynamic> persona = {
    'nombre': 'Juan',
    'edad': 35,
    'soltero': false,
    'true': false,
    '1': 100,
    '2': 500
  };
  
  persona.addAll({ '3': 'Juan' });
  
  print(persona);

  
}
