import 'dart:math' as math;

void main() {
  final cuadrado = new Cuadrado(5);

  print('area ${cuadrado.calcularArea()}');

    cuadrado.area = 100;

  
  print('lado ${cuadrado.lado}');

  print('area ${cuadrado.area}');
  
  
}

class Cuadrado {
  double lado = 0;

  double get area {
    return this.lado * this.lado;
  }

   set area(double valor) {
     this.lado = math.sqrt(valor);
   }
  
  Cuadrado(double lado) : this.lado = lado;

  double calcularArea() {
    return this.lado * this.lado;
  }
}
