void main() {
//   Strings

//   var nombre = 'Tony';
//   var apellido = 'Stark';

//   String nombre = 'Peter';
//   String apellido = 'Parker';

  final String nombre = 'Peter';
  const apellido = 'Parker';

  print('$nombre $apellido');

  // Numeros
  int empleados = 10;
  double salario = 1865.25;
  
  print(empleados);
  print(salario);
}
