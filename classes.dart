void main() {
  final wolverine = Heroe(nombre: 'Peter', poder: 'Tela-araña');

  print(wolverine);
}

class Heroe {
  String? nombre;
  String? poder;

  Heroe({required this.nombre, required this.poder});

  @override
  String toString() {
    return 'nombre: ${this.nombre}, poder ${this.poder}';
  }
}
